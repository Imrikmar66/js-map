import mapboxgl from 'mapbox-gl/dist/mapbox-gl.js';
import CATEGORY from '../CATEGORY';
import detail from './Detail';

export class Event {

    constructor( title, description, position ) {

        this.$popup = null;
        this.$button = null;

        this.title = title;
        this.description = description;
        this.position = position;
        this.categories = [];

        this.marker = null;
        this.popup = new mapboxgl.Popup({ offset: 25 });
    }

    addCategory( category ) {
        this.categories.push( category );
    }

    createPopup() {

        this.$popup = document.createElement('div');

        let html = `
            <h3> ${this.title} </h3>
            <h4> Categories: </h4>
            <ul>
        `;
        for( let category_id of this.categories ) {
            html += `<li> ${CATEGORY[category_id]} </li>`;
        }
        html += '</ul>';

        this.$popup.innerHTML = html;

        this.$button = document.createElement('button');
        this.$button.textContent = 'Voir le détail';

        this.$popup.append( this.$button );

        this.popup
            .setDOMContent( this.$popup );
        
    }

    bindEvents() {

        this.$button.addEventListener('click', () => {
            
            detail.show( this );

        });

    }

    display( map ) {

        this.createPopup();
        this.bindEvents();

        const $el = document.createElement('div');

        const key = this.categories[0];
        $el.className=`custom-marker ${CATEGORY[key]}`;

        this.marker = new mapboxgl.Marker( $el );

        this.marker
            .setLngLat( this.position )
            .setPopup( this.popup )
            .addTo( map );
        
    }

}