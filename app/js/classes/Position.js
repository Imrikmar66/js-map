export class Position {

    constructor( lat, lng ) {
        this.lat = lat;
        this.lng = lng;
    }

    check() {
        return this.lat < 90
            && this.lat > -90
            && this.lng < 180
            && this.lng > -180;
    }

}