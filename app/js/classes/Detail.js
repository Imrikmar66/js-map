import CATEGORY from "../CATEGORY";

class Detail {

    constructor() {

        this.$dom         = document.getElementById('detail');
        this.$close       = this.$dom.children[0];
        this.$title       = this.$dom.children[1];
        this.$description = this.$dom.children[2];
        this.$address     = this.$dom.children[3];
        this.$categories  = this.$dom.children[4];

        this.bindEvents();

    }

    bindEvents() {
        this.$close.addEventListener('click', () => {
            this.hide();
        });
    }


    show( event ) {
        this.$title.textContent = event.title;
        this.$description.textContent = event.description;

        let html = '';
        for( let category_id of event.categories ) {
            html += `<li> ${CATEGORY[category_id]} </li>`;
        }
        this.$categories.innerHTML = html;

        this.$dom.classList.add('active');
    }

    hide() {
        this.$dom.classList.remove('active');
    }

}

export default new Detail;