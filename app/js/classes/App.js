import mapboxgl from 'mapbox-gl/dist/mapbox-gl.js';
import config from "../../config";

class App {

    constructor() {

        this.$loader = document.getElementById('loader');

        this.$form = document.getElementById('form-events');
        this.$input_event = document.getElementById('event');
        this.$input_description = document.getElementById('description');
        this.$input_lat = document.getElementById('latitude');
        this.$input_lng = document.getElementById('longitude');
        this.$checkboxes = document.getElementsByClassName('category');

        this.map = null;
        this.initMap();

        this.me = new mapboxgl.Marker();

    }

    initMap() {

        mapboxgl.accessToken = config.accessToken;
        this.map = new mapboxgl.Map({
            container: config.container,
            style: config.style
        });
    }

}

export default new App;