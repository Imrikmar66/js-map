import '../scss/styles';
import app from './classes/App';
import { Position } from './classes/Position';
import { Event } from './classes/Event';

app.map.on('load', () => {
    
    app.$loader.classList.add('fade');

    setTimeout(() => {
        app.$loader.classList.add('hide');
    }, 500);

});

navigator
    .geolocation
    .getCurrentPosition( 
        function( geopos ) {

            const position = new Position(
                geopos.coords.latitude,
                geopos.coords.longitude
            );

            app.map
                .setZoom(15)
                .panTo( position );

            app.me
                .setLngLat( position )
                .addTo( app.map );  


        }
);

app.map.on('click', function (event) {

    app.$input_lat.value = event.lngLat.lat;
    app.$input_lng.value = event.lngLat.lng;

});

app.$form.addEventListener('submit', function( e ){

    e.preventDefault();

    const title = app.$input_event.value;
    const description = app.$input_description.value;
    const position = new Position(
        app.$input_lat.value,
        app.$input_lng.value
    );

    if(
        title.length > 0
        && description.length > 12
        && position.check()
    ) {

        const event = new Event( title, description, position );

        for( let $checkbox of app.$checkboxes ) {

            if( $checkbox.checked ) {
                event.addCategory( $checkbox.value );
            }

        }

        event.display( app.map );
    }

});